package com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class MergeSortTest {


    @Test
    public void testMergeSort() throws Exception {

        //Create array to sort
        Double testTable[] = {1D,15D,3D,45D,8D,6D,4D,12D};

        //Create sorted array to equal
        Double sortTestTable [] = {1D,3D,4D,6D,8D,12D,15D,45D};

        //Run tested method
        Comparable result[] = MergeSort.mergeSort(testTable);

        //Checking result
        for(int i = 0; i < sortTestTable.length; i++) {
            assertTrue(result[i].equals(sortTestTable[i]));
        }
    }
}