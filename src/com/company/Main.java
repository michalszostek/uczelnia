package com.company;

import java.util.Arrays;

public class Main {


    public static double greatestMultiplayOfStringNumber(String liczba){

        double mul = 0;

        double number1, number2, number3;

        for(int j=0; j < (liczba.length()-3);j++) {
            for (int i = j; i < j+2; i++) {
                number1 = (double)Character.digit(liczba.charAt(i),10);
                number2 = (double)Character.digit(liczba.charAt(i+1),10);
                number3 = (double)Character.digit(liczba.charAt(i+2),10);
                if (mul < number1 * number2 * number3) {
                    mul = number1 * number2 * number3;
                }
            }
        }
        return mul;
    }

    public static void main(String[] args) {

        //Call method and print result
        System.out.println(greatestMultiplayOfStringNumber("123456789"));

        //Unsorted array
        Integer[] a = { 2, 6, 3, 5, 1 };

        //Call merge sort
        MergeSort.mergeSort(a);

        //Print result
        System.out.println(Arrays.toString(a));

    }
}
