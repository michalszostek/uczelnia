package com.company;

public class MergeSort {

    public static Comparable[] mergeSort(Comparable[] list) {

        //If list is empty, do nothing
        if (list.length <= 1) {
            return list;
        }

        //Split the array in half in two parts
        Comparable[] firstArray = new Comparable[list.length / 2];
        Comparable[] secondArray = new Comparable[list.length - firstArray.length];
        System.arraycopy(list, 0, firstArray, 0, firstArray.length);
        System.arraycopy(list, firstArray.length, secondArray, 0, secondArray.length);

        //Sort each half recursively
        mergeSort(firstArray);
        mergeSort(secondArray);

        //Merge both halves together, overwriting to original array
        merge(firstArray, secondArray, list);
        return list;
    }

    private static void merge(Comparable[] first, Comparable[] second, Comparable[] result) {

        //Index Position in first array - starting with first element
        int iFirst = 0;

        //Index Position in second array - starting with first element
        int iSecond = 0;

        //Index Position in merged array - starting with first position
        int iMerged = 0;

        //Compare elements at iFirst and iSecond,and move smaller element at iMerged
        while (iFirst < first.length && iSecond < second.length) {
            if (first[iFirst].compareTo(second[iSecond]) < 0) {
                result[iMerged] = first[iFirst];
                iFirst++;
            } else {
                result[iMerged] = second[iSecond];
                iSecond++;
            }
            iMerged++;
        }

        //Copy remaining elements from both halves - each half will have already sorted elements
        System.arraycopy(first, iFirst, result, iMerged, first.length - iFirst);
        System.arraycopy(second, iSecond, result, iMerged, second.length - iSecond);
    }
}
